# fullstack-dev-test

Test project for the fullstack position

### 1. Close the repository
```$xslt
git clone https://gitlab.com/korimarik/fullstack-dev-test.git
```

### 2. Install Composer dependencies
```$xslt
composer install
```

### 3. Create a MySQL database(e.g. `fullstack`)

### 4. Rename `.env.example` to `.env` file in the root directory.

### 5. Adjust the following settings:
```$xslt
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<database_name>
DB_USERNAME=<username_of_your_server>
DB_PASSWORD=<password_of_your_server>
```

### 6. Migrate tables of the Database
```$xslt
php artisan migrate
```

### 7. Seed the Database
```$xslt
php artisan db:seed
```

### 8. Generate application key
```$xslt
php artisan key:generate
```

### 9. Start backend server
```$xslt
php artisan serve
```

### 10. Open the following url
```$xslt
http://127.0.0.1:8000/
```
