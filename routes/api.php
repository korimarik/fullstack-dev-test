<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::get('/items', 'ItemController@index');
    Route::post('/items', 'ItemController@store');
    Route::put('/items', 'ItemController@update');
    Route::delete('/items', 'ItemController@destroy');
});
