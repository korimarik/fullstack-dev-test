<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    /**
     * Display a listing of the resources.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $items = Item::all();

        return response()->json($items);
    }

    /**
     * Store a newly created resources in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $items = $request->all('items');
        $items = $items['items'];
        foreach($items as $item) {
            Item::create($item);
        }

        return response()->json('OK. Created!', 201);
    }

    /**
     * Update the specified resources in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $items = $request->all('items');
        $items = $items['items'];
        foreach ($items as $item) {
            Item::find($item['id'])->update($item);
        }

        return response()->json('OK. Updated', 200);
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $items = $request->all('items');
        $items = $items['items'];
        Item::destroy($items);

        return response()->json('OK. Destroyed', 200);
    }
}
